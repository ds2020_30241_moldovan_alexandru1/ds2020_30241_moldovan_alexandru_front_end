import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    caregiver: '/medication/'
};

function getMedications(callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function saveMedication(medication, callback) {
    let methodType = medication.id ? 'PUT' : 'POST';
    let request = new Request(HOST.backend_api + endpoint.caregiver, {
        method: methodType,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medication)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteMedication(medicationId, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + medicationId, {
        method: 'DELETE'
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);

}

export {
    getMedications,
    getMedicationById,
    saveMedication,
    deleteMedication
};
