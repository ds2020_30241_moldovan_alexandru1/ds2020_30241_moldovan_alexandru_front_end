import {HOST} from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    medication_plan: '/medication-plan/'
};


function getMedicationPlanById(medicationPlanId, callback) {
    let request = new Request(HOST.backend_api + endpoint.medication_plan + medicationPlanId, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getMedicationPlanById
}