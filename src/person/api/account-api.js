import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    account: '/account/'
};


function getAccountByUsername(username, callback) {
    let request = new Request(HOST.backend_api + endpoint.account + username, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function saveAccount(account, callback) {
    console.log(account);
    let request = new Request(HOST.backend_api + endpoint.account, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(account)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getAccountByUsername,
    saveAccount,
};
