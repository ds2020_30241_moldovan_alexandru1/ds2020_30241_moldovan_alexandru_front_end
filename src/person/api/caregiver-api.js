import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    caregiver: '/caregiver/',
    caregiverUsername: '/caregiver/username?username='
};

function getCaregivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregiverById(caregiverId, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + caregiverId, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function saveCaregiver(caregiver, callback) {
    let methodType = caregiver.id ? 'PUT' : 'POST';
    let request = new Request(HOST.backend_api + endpoint.caregiver, {
        method: methodType,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteCaregiver(caregiverId, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + caregiverId, {
        method: 'DELETE'
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);

}

function getCaregiverByUsername(username, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiverUsername + username, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getCaregivers,
    getCaregiverById,
    saveCaregiver,
    deleteCaregiver,
    getCaregiverByUsername
};
