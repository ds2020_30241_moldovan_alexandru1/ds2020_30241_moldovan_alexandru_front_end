import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patient/',
    patientUsername: '/patient/username?username='
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.patient, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientById(patientId, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + patientId, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function savePatient(patient, callback) {
    console.log(patient);
    let methodType = patient.id ? 'PUT' : 'POST';
    let request = new Request(HOST.backend_api + endpoint.patient, {
        method: methodType,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deletePatient(patientId, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + patientId, {
        method: 'DELETE'
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);

}

function addMedicationPlan(patientId, medicationPlan, callback) {
    console.log(medicationPlan);
    let request = new Request(HOST.backend_api + endpoint.patient + patientId, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicationPlan)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


function getPatientByUsername(username, callback) {
    let request = new Request(HOST.backend_api + endpoint.patientUsername + username, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


export {
    getPatients,
    getPatientById,
    savePatient,
    deletePatient,
    addMedicationPlan,
    getPatientByUsername
};
