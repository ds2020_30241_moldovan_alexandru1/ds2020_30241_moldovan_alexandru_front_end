import React from 'react';
import validate from "../validators/person-validators";
import Button from "react-bootstrap/Button";
import * as API_PATIENT from "../../api/patient-api";
import APIResponseErrorMessage from "../../../commons/errorhandling/api-response-error-message";
import {Col, FormGroup, Input, Label, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import Select from 'react-select'

class MedicationPlanForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleMedicationModal = this.toggleMedicationModal.bind(this);
        this.medications = this.props.medications;
        this.getCurrentMedicationPlan(this.props.selectedPatient);
        this.state = {

            medicationSelected: false,

            selectedMedications: [],

            isLoaded: true,

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                id: {
                    value: null,
                    valid: true
                },
                period: {
                    value: "",
                    placeholder: '10 zile, 1 luna, 3 luni...',
                    valid: false,
                    touched: false,
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    onInputChange = selected => {
        this.setState({
            selectedMedications: selected,
            medicationSelected: !this.state.medicationSelected
        });
    };


    initMedicationPlanForm(medicationPlan) {
        console.log(medicationPlan);
        let medicationsSelected = [];
        if (medicationPlan != null) {
            medicationPlan.medications.map((medication) => {
                    console.log(medication);
                    medicationsSelected.push(
                        {
                            data: medication,
                            label: medication.medication.name,
                            value: medication.id
                        }
                    )
                }
            )

            console.log(medicationsSelected);
            this.setState(
                this.state = {
                    isLoaded: true,
                    selectedMedications: medicationsSelected,
                    formIsValid: true,
                    formControls: {
                        id: {
                            value: medicationPlan.id,
                            valid: true
                        },

                        period: {
                            value: medicationPlan.period,
                            placeholder: '10 zile, 1 luna, 3 luni...',
                            valid: true,
                            touched: false,
                        }
                    }
                }
            )
        }
    }

    getCurrentMedicationPlan(id) {
        console.log(id);
        return API_PATIENT.getPatientById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.initMedicationPlanForm(result.medicationPlanDTO);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    toggleMedicationModal() {
        this.setState({
            medicationSelected: !this.state.medicationSelected
        })
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    saveMedicationPlan(medicationPlan) {
        // return API_CAREGIVER.saveMedicationPlan(medicationPlan, (result, status, error) => {
        //     if (result !== null && (status === 200 || status === 201)) {
        //         console.log("Successfully inserted medicationPlan with id: " + result);
        //     } else {
        //         this.setState(({
        //             errorStatus: status,
        //             error: error
        //         }));
        //     }
        // });
    }

    handleSubmit() {
        let medicationsToAdd = [];
        if (this.state.selectedMedications) {
            this.state.selectedMedications
                .map((val) => medicationsToAdd.push(val.data));
        }
        let medication = {
            id: this.state.formControls.id.value,
            period: this.state.formControls.period.value,
            medications: medicationsToAdd
        };
        this.saveMedicationPlan(medication);
    }

    clickOnItem(){
        this.setState({
            medicationSelected: !this.state.medicationSelected
        });
    }

    render() {
        return (
            <div>
                {
                    !this.state.isLoaded
                        ? <span>Loading</span>
                        : <div>

                            <FormGroup id='period'>
                                <Label for='periodField'> Period: </Label>
                                <Input name='period' id='periodField'
                                       placeholder={this.state.formControls.period.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.period.value}
                                       touched={this.state.formControls.period.touched ? 1 : 0}
                                       valid={this.state.formControls.period.valid}
                                       required
                                />
                            </FormGroup>

                            <FormGroup id='medications'>
                                <Label for='medicationsField'> Medications: </Label>
                                <Select
                                    isMulti
                                    defaultValue={this.state.selectedMedications}
                                    value={this.state.selectedMedications}
                                    onChange={this.onInputChange}
                                    name="color"
                                    options={this.medications}
                                />
                            </FormGroup>

                            <Row>
                                <Col sm={{size: '4', offset: 8}}>
                                    <Button type={"submit"} disabled={!this.state.formIsValid}
                                            onClick={this.handleSubmit}> Submit </Button>
                                </Col>
                            </Row>

                            {
                                this.state.errorStatus > 0 &&
                                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                            }

                            <Modal isOpen={this.state.medicationSelected} toggle={this.toggleMedicationModal}
                                   className={this.props.className} size="lg">
                                <ModalHeader toggle={this.toggleMedicationModal}> Info about: </ModalHeader>
                                <ModalBody>
                                    {this.medications}
                                </ModalBody>
                            </Modal>
                        </div>
                }
            </div>
        );
    }
}

export default MedicationPlanForm;
