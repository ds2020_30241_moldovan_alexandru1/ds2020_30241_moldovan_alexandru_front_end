import React from 'react';
import validate from "../validators/person-validators";
import Button from "react-bootstrap/Button";
import * as API_CAREGIVER from "../../api/caregiver-api";
import APIResponseErrorMessage from "../../../commons/errorhandling/api-response-error-message";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import Select from 'react-select'
import * as API_ACCOUNT from "../../api/account-api";

class CaregiverForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler
        this.patients = this.props.patients;
        console.log(this.patients);
        if (this.props.row) {
            this.getCurrentCaregiver(this.props.row.id);
        }
        this.state = {

            selectedPatients: [],

            isLoaded: true,

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                id: {
                    value: null,
                    valid: true
                },
                name: {
                    value: "",
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                age: {
                    value: "",
                    placeholder: 'Age...',
                    valid: false,
                    touched: false,
                },
                address: {
                    value: "",
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    valid: false,
                    touched: false,
                },
                gender: {
                    value: "",
                    placeholder: 'What is your gender?',
                    valid: false,
                    touched: false,
                },
                username: {
                    value: "",
                    placeholder: 'Choose a username',
                    valid: false,
                    touched: false
                },
                password: {
                    value: "",
                    placeholder: 'Choose a password',
                    valid: false,
                    touched: false
                },
            }

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    onInputChange = selected => {
        this.setState({selectedPatients: selected});
    };


    initCaregiverForm(caregiver) {
        let patientsSelected = [];
        caregiver.patients.map((patient) =>
            patientsSelected.push(
                {
                    data: patient,
                    label: patient.name,
                    value: patient.id
                }
            )
        )
        console.log(patientsSelected);
        this.setState(
            this.state = {
                isLoaded: true,
                selectedPatients: patientsSelected,
                formIsValid: true,
                formControls: {
                    id: {
                        value: caregiver.id,
                        valid: true
                    },
                    name: {
                        value: caregiver.name,
                        placeholder: 'What is your name?...',
                        valid: true,
                        touched: false,
                        validationRules: {
                            minLength: 3,
                            isRequired: true
                        }
                    },
                    age: {
                        value: caregiver.age,
                        placeholder: 'Age...',
                        valid: true,
                        touched: false,
                    },
                    address: {
                        value: caregiver.address,
                        placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                        valid: true,
                        touched: false,
                    },
                    gender: {
                        value: caregiver.gender,
                        placeholder: 'What is your gender?',
                        valid: true,
                        touched: false,
                    },
                    username: {
                        value: caregiver.username,
                        placeholder: 'Choose a username',
                        valid: true,
                        touched: false
                    },
                    password: {
                        value: caregiver.password,
                        placeholder: 'Choose a password',
                        valid: true,
                        touched: false
                    },
                    medicalRecord: {
                        value: caregiver.medicalRecord,
                        placeholder: 'What is your problem?',
                        valid: true,
                        touched: false
                    }
                }
            }
        )
    }

    getCurrentCaregiver(id) {
        return API_CAREGIVER.getCaregiverById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.initCaregiverForm(result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }


    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    saveCaregiver(caregiver) {
        return API_CAREGIVER.saveCaregiver(caregiver, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted caregiver with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        console.log(this.state.selectedPatients);
        let patientsToAdd = [];
        if (this.state.selectedPatients) {
            this.state.selectedPatients
                .map((val) => patientsToAdd.push(val.data));
        }
        let caregiver = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            age: this.state.formControls.age.value,
            address: this.state.formControls.address.value,
            gender: this.state.formControls.gender.value,
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
            patients: patientsToAdd
        };

        let account = {
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
            role: 'caregiver'
        };
        this.registerAccount(account);
        this.saveCaregiver(caregiver);
    }

    registerAccount(account) {
        return API_ACCOUNT.saveAccount(account, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log(result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    render() {
        return (
            <div>
                {
                    !this.state.isLoaded
                        ? <span>Loading</span>
                        : <div>
                            <FormGroup id='name'>
                                <Label for='nameField'> Name: </Label>
                                <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.name.value}
                                       touched={this.state.formControls.name.touched ? 1 : 0}
                                       valid={this.state.formControls.name.valid}
                                       required
                                />
                                {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                                <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                            </FormGroup>

                            <FormGroup id='address'>
                                <Label for='addressField'> Address: </Label>
                                <Input name='address' id='addressField'
                                       placeholder={this.state.formControls.address.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.address.value}
                                       touched={this.state.formControls.address.touched ? 1 : 0}
                                       valid={this.state.formControls.address.valid}
                                       required
                                />
                            </FormGroup>

                            <FormGroup id='age'>
                                <Label for='ageField'> Age: </Label>
                                <Input name='age' id='ageField' placeholder={this.state.formControls.age.placeholder}
                                       min={0} max={100} type="number"
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.age.value}
                                       touched={this.state.formControls.age.touched ? 1 : 0}
                                       valid={this.state.formControls.age.valid}
                                       required
                                />
                            </FormGroup>

                            <FormGroup id='gender'>
                                <Label for='genderField'> Gender: </Label>
                                <Input name='gender' id='genderField'
                                       placeholder={this.state.formControls.gender.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.gender.value}
                                       touched={this.state.formControls.gender.touched ? 1 : 0}
                                       valid={this.state.formControls.gender.valid}
                                       required
                                />
                            </FormGroup>

                            <FormGroup id='username'>
                                <Label for='usernameField'> Username: </Label>
                                <Input name='username' id='usernameField'
                                       placeholder={this.state.formControls.username.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.username.value}
                                       touched={this.state.formControls.username.touched ? 1 : 0}
                                       valid={this.state.formControls.username.valid}
                                       required
                                />
                            </FormGroup>

                            <FormGroup id='password'>
                                <Label for='passwordField'> Password: </Label>
                                <Input name='password' id='passwordField'
                                       placeholder={this.state.formControls.password.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.password.value}
                                       touched={this.state.formControls.password.touched ? 1 : 0}
                                       valid={this.state.formControls.password.valid}
                                       required
                                />
                            </FormGroup>

                            <FormGroup id='patients'>
                                <Label for='patientField'> Patients: </Label>
                                <Select
                                    isMulti
                                    defaultValue={this.state.selectedPatients}
                                    value={this.state.selectedPatients}
                                    onChange={this.onInputChange}
                                    name="color"
                                    options={this.patients}
                                />
                            </FormGroup>

                            <Row>
                                <Col sm={{size: '4', offset: 8}}>
                                    <Button type={"submit"} disabled={!this.state.formIsValid}
                                            onClick={this.handleSubmit}> Submit </Button>
                                </Col>
                            </Row>

                            {
                                this.state.errorStatus > 0 &&
                                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                            }
                        </div>
                }
            </div>
        );
    }
}

export default CaregiverForm;
