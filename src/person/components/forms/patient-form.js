import React from 'react';
import validate from "../validators/person-validators";
import Button from "react-bootstrap/Button";
import * as API_PATIENT from "../../api/patient-api";
import * as API_ACCOUNT from "../../api/account-api";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import APIResponseErrorMessage from "../../../commons/errorhandling/api-response-error-message";


class PatientForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        if (this.props.row) {
            this.row = this.props.row;
            this.getCurrentPatient(this.row.id);
        }
        this.state = {

            isLoaded: true,

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                id: {
                    value: null,
                    valid: true
                },
                name: {
                    value: "",
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                age: {
                    value: "",
                    placeholder: 'Age...',
                    valid: false,
                    touched: false,
                },
                address: {
                    value: "",
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    valid: false,
                    touched: false,
                },
                gender: {
                    value: "",
                    placeholder: 'What is your gender?',
                    valid: false,
                    touched: false,
                },
                username: {
                    value: "",
                    placeholder: 'Choose a username',
                    valid: false,
                    touched: false
                },
                password: {
                    value: "",
                    placeholder: 'Choose a password',
                    valid: false,
                    touched: false
                },
                medicalRecord: {
                    value: "",
                    placeholder: 'What is your problem?',
                    valid: false,
                    touched: false
                }
            }
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    initPatientForm(patient) {
        console.log(patient);
        this.setState(
            this.state = {
                isLoaded: true,
                formControls: {
                    id: {
                        value: patient.id,
                        valid: true
                    },
                    name: {
                        value: patient.name,
                        placeholder: 'What is your name?...',
                        valid: true,
                        touched: false,
                        validationRules: {
                            minLength: 3,
                            isRequired: true
                        }
                    },
                    age: {
                        value: patient.age,
                        placeholder: 'Age...',
                        valid: true,
                        touched: false,
                    },
                    address: {
                        value: patient.address,
                        placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                        valid: true,
                        touched: false,
                    },
                    gender: {
                        value: patient.gender,
                        placeholder: 'What is your gender?',
                        valid: true,
                        touched: false,
                    },
                    username: {
                        value: patient.username,
                        placeholder: 'Choose a username',
                        valid: true,
                        touched: false
                    },
                    password: {
                        value: patient.password,
                        placeholder: 'Choose a password',
                        valid: true,
                        touched: false
                    },
                    medicalRecord: {
                        value: patient.medicalRecord,
                        placeholder: 'What is your problem?',
                        valid: true,
                        touched: false
                    }
                }
            }
        )
    }

    getCurrentPatient(id) {
        return API_PATIENT.getPatientById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log(result);
                this.initPatientForm(result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registerPatient(patient) {
        return API_PATIENT.savePatient(patient, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log(result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }


    registerAccount(account) {
        return API_ACCOUNT.saveAccount(account, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log(result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }


    handleSubmit() {
        let patient = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            age: this.state.formControls.age.value,
            address: this.state.formControls.address.value,
            gender: this.state.formControls.gender.value,
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
            medicalRecord: this.state.formControls.medicalRecord.value
        };

        let account = {
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
            role: "patient"
        };

        console.log(patient);
        this.registerPatient(patient);
        this.registerAccount(account);
    }

    render() {
        return (
            <div>
                {
                    !this.state.isLoaded
                        ? <span>Loading.....</span>
                        : <div>
                            <FormGroup id='name'>
                                <Label for='nameField'> Name: </Label>
                                <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.name.value}
                                       touched={this.state.formControls.name.touched ? 1 : 0}
                                       valid={this.state.formControls.name.valid}
                                       required
                                />
                                {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                                <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                            </FormGroup>

                            <FormGroup id='address'>
                                <Label for='addressField'> Address: </Label>
                                <Input name='address' id='addressField'
                                       placeholder={this.state.formControls.address.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.address.value}
                                       touched={this.state.formControls.address.touched ? 1 : 0}
                                       valid={this.state.formControls.address.valid}
                                       required
                                />
                            </FormGroup>

                            <FormGroup id='age'>
                                <Label for='ageField'> Age: </Label>
                                <Input name='age' id='ageField' placeholder={this.state.formControls.age.placeholder}
                                       min={0} max={100} type="number"
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.age.value}
                                       touched={this.state.formControls.age.touched ? 1 : 0}
                                       valid={this.state.formControls.age.valid}
                                       required
                                />
                            </FormGroup>

                            <FormGroup id='gender'>
                                <Label for='genderField'> Gender: </Label>
                                <Input name='gender' id='genderField'
                                       placeholder={this.state.formControls.gender.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.gender.value}
                                       touched={this.state.formControls.gender.touched ? 1 : 0}
                                       valid={this.state.formControls.gender.valid}
                                       required
                                />
                            </FormGroup>

                            <FormGroup id='username'>
                                <Label for='usernameField'> Username: </Label>
                                <Input name='username' id='usernameField'
                                       placeholder={this.state.formControls.username.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.username.value}
                                       touched={this.state.formControls.username.touched ? 1 : 0}
                                       valid={this.state.formControls.username.valid}
                                       required
                                />
                            </FormGroup>

                            <FormGroup id='password'>
                                <Label for='passwordField'> Password: </Label>
                                <Input name='password' id='passwordField'
                                       placeholder={this.state.formControls.password.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.password.value}
                                       touched={this.state.formControls.password.touched ? 1 : 0}
                                       valid={this.state.formControls.password.valid}
                                       required
                                />
                            </FormGroup>

                            <FormGroup id='medicalRecord'>
                                <Label for='medicalRecordField'> Medical record: </Label>
                                <Input name='medicalRecord' id='medicalRecordField'
                                       placeholder={this.state.formControls.medicalRecord.placeholder}
                                       onChange={this.handleChange}
                                       defaultValue={this.state.formControls.medicalRecord.value}
                                       touched={this.state.formControls.medicalRecord.touched ? 1 : 0}
                                       valid={this.state.formControls.medicalRecord.valid}
                                       required
                                />
                            </FormGroup>

                            <Row>
                                <Col sm={{size: '4', offset: 8}}>
                                    <Button type={"submit"} disabled={!this.state.formIsValid}
                                            onClick={this.handleSubmit}> Submit </Button>
                                </Col>
                            </Row>
                            {
                                this.state.errorStatus > 0 &&
                                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                            }
                        </div>
                }
            </div>
        );
    }
}

export default PatientForm;
