import React from "react";
import Table from "../../../commons/tables/table";
import * as API_MEDICATION from "../../api/medication-api";

class MedicationTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData,
        };
        this.parentCallBack = this.props.parentCallBack;
        this.reloadDataHandler = this.props.reloadDataHandler;
    }

    columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Side effects',
            accessor: 'sideEffects',
        },
        {
            Header: 'Dosage',
            accessor: 'dosage',
        },
        {
            Header: '',
            Cell: row => (
                <div>
                    <button onClick={() => this.handleEdit(row.original)}>Edit</button>
                    <button onClick={() => this.handleDelete(row.original)}>Delete</button>
                </div>
            )
        }
    ];

    filters = [
        {
            accessor: 'name',
        }
    ];

    handleEdit(row) {
        this.parentCallBack(true, row);
    }

    handleDelete(row) {
        return API_MEDICATION.deleteMedication(row.id, (result, status, error) => {
            if (status === 200 || status === 201) {
                console.log("Successfully deleted medication: ");
                this.reloadDataHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={this.columns}
                search={this.filters}
                pageSize={5}
            />
        )
    }
}

export default MedicationTable;