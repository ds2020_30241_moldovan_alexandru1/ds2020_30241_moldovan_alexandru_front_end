import React from "react";
import Table from "../../../commons/tables/table";
import JsonRpcClient from "react-jsonrpc-client";
import {HOST} from "../../../commons/hosts";

class MedicationPlanTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: false,
            tableData: this.props.tableData
        };
        console.log(this.props.tableData)
    }

    columns = [
        {
            Header: 'Name',
            accessor: 'medication.name'
        },
        {
            id: 'morning',
            Header: 'Morning',
            accessor: d => d.morning === true ? "true" : "false"
        },
        {
            id: 'evening',
            Header: 'Evening',
            accessor: d => d.evening === true ? "true" : "false"
        },
        {
            id: 'noon',
            Header: 'Noon',
            accessor: d => d.noon === true ? "true" : "false"
        },
        {
            Header: '',
            Cell: row => (
                <div>{
                    <button onClick={() => this.handleTake(row.original)}>Take medication</button>}
                </div>
            )
        }
    ];

    filters = [
        {
            accessor: 'name',
        }
    ];

    handleTake(row) {
        let index = this.state.tableData.indexOf(row)
        let intakeInterval = this.state.tableData[index]
        intakeInterval.taken = true
        this.state.tableData[index] = intakeInterval
        this.marAsTaken(row.id)
    }

    marAsTaken(id){
        let headers = new Headers();
        headers.append('Access-Control-Allow-Origin', 'http://localhost:3000');
        const api = new JsonRpcClient({
            endpoint: HOST.backend_api + "/api",
            headers: headers
        });
        api.request(
            "markAsTaken",
            id
        ).then(function (response) {
            this.setState({
            })
        }.bind(this))
    }

    markAsNotTaken(id){
        let headers = new Headers();
        headers.append('Access-Control-Allow-Origin', 'http://localhost:3000');
        const api = new JsonRpcClient({
            endpoint: HOST.backend_api + "/api",
            headers: headers
        });
        api.request(
            "markAsNotTaken",
            id
        ).then(function (response) {
            this.setState({
            })
        }.bind(this))
    }

    componentDidMount() {
        setInterval(
            () => this.tick(),
            1000
        );
    }


    tick() {
        this.setState({
            time: new Date().getSeconds() + '  ' + new Date().getMinutes()
        });
        this.checkMedication()
    }

    checkMedication() {
        this.state.tableData.map(
            intakeInterval => {
               let index = this.state.tableData.indexOf(intakeInterval)
                if (intakeInterval.morning === true && new Date().getSeconds() > 20 && intakeInterval.taken === false) {
                    console.log('Nu a luat medicamentul de dimineata')
                    this.markAsNotTaken(this.state.tableData[index].id)
                    this.state.tableData.splice(this.state.tableData.indexOf(intakeInterval), 1)
                }
                if (intakeInterval.evening === true && new Date().getSeconds() > 40 && intakeInterval.taken === false) {
                    console.log('Nu a luat medicamentul la amiaz')
                    this.markAsNotTaken(this.state.tableData[index].id)
                    this.state.tableData.splice(this.state.tableData.indexOf(intakeInterval), 1)
                }
                if (intakeInterval.noon === true && new Date().getSeconds() > 58 && intakeInterval.taken === false) {
                    console.log('Nu a luat medicamentul seara')
                    this.markAsNotTaken(this.state.tableData[index].id)
                    this.state.tableData.splice(this.state.tableData.indexOf(intakeInterval), 1)
                }
            }
        )
    }

    render() {
        return (
            <div>
                <p>
                    {this.state.time}
                </p>
                <Table
                    data={this.state.tableData}
                    columns={this.columns}
                    search={this.filters}
                    pageSize={5}
                />
            </div>
        )
    }
}

export default MedicationPlanTable