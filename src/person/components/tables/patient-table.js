import React from "react";
import Table from "../../../commons/tables/table";
import * as API_PATIENT from "../../api/patient-api";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import MedicationPlanForm from "../forms/medication-plan-form";

class PatientTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: false,
            tableData: this.props.tableData,
            selectedPatient: null
        };
        this.toggleForm = this.toggleForm.bind(this);
        this.parentCallBack = this.props.parentCallBack;
        this.reloadDataHandler = this.props.reloadDataHandler;
    }

    columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Age',
            accessor: 'age',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Medical record',
            accessor: 'medicalRecord',
        },
        {
            Header: '',
            Cell: row => (
                <div>
                    <button onClick={() => this.handleEdit(row.original)}>Edit</button>
                    <button onClick={() => this.handleDelete(row.original)}>Delete</button>
                </div>
            )
        },
        {
            Header: '',
            Cell: row => (
                <div>
                    <button onClick={() => this.toggleForm(row.original)}>Medication plan</button>
                </div>
            )
        }
    ];

    filters = [
        {
            accessor: 'name',
        }
    ];

    handleEdit(row) {
        this.parentCallBack(true, row);
    }

    handleDelete(row) {
        return API_PATIENT.deletePatient(row.id, (result, status, error) => {
            if (status === 200 || status === 201) {
                console.log("Successfully deleted patient: ");
                this.reloadDataHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    toggleForm(row) {
        console.log(this.state.selected);
        this.setState({
            selected: !this.state.selected,
            selectedPatient: row.id
        });
    }

    render() {
        return (
            <div>
                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Create a medication plan
                        for {this.state.selectedPatient} </ModalHeader>
                    <ModalBody>
                        <MedicationPlanForm selectedPatient={this.state.selectedPatient}/>
                    </ModalBody>
                </Modal>
                <Table
                    data={this.state.tableData}
                    columns={this.columns}
                    search={this.filters}
                    pageSize={5}
                />
            </div>
        )
    }
}

export default PatientTable;