import React from 'react';
import {Form, Grid, Header, Message} from 'semantic-ui-react';
import {Helmet} from 'react-helmet';
import store from 'store';
import styles from './styles.css';
import * as API_ACCOUNT from '../../src/person/api/account-api';

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            error: false,
        };
        this.account = null;

        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();

        const {username, password} = this.state;

        this.setState({error: false});

        if (username === "doctor" && password === "doctor")
            this.props.history.push("/doctor");

        this.getAccount(username);
        store.set('loggedIn', true);

    }

    handleChange(e, {name, value}) {
        this.setState({[name]: value});
    }

    getAccount(username) {
        return API_ACCOUNT.getAccountByUsername(username, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.account = result;
                if (this.account.role === "patient")
                    this.props.history.push(`patient/${this.account.username}`);
                if (this.account.role === "caregiver")
                    this.props.history.push(`caregiver/${this.account.username}`);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    render() {
        const {error} = this.state;

        return (
            <Grid>
                <Helmet>
                    <title>CMS | Login</title>
                </Helmet>

                <Grid.Column width={6}/>
                <Grid.Column width={4}>
                    <Form className={styles.loginForm} error={error} onSubmit={this.onSubmit}>
                        <Header as="h1">Login</Header>
                        {error && <Message
                            error={error}
                            content="That username/password is incorrect. Try again!"
                        />}
                        <Form.Input
                            inline
                            label="Username"
                            name="username"
                            onChange={this.handleChange}
                        />
                        <Form.Input
                            inline
                            label="Password"
                            type="password"
                            name="password"
                            onChange={this.handleChange}
                        />
                        <Form.Button type="submit">Go!</Form.Button>
                    </Form>
                </Grid.Column>
            </Grid>
        );
    }
}

export default Login;
