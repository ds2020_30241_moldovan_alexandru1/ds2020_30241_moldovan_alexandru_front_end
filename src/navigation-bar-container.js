import React from 'react'
import logo from './commons/images/icon.png';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const
    textStyle = {
        color: 'white',
        textDecoration: 'none'
    };

class NavigationBarContainer extends React.Component {

    constructor(props) {
        super(props);
        this.user = this.props.user;
        console.log(this.user);
    }

    render() {
        return (
            <div>
                <Navbar color="dark" light expand="md">
                    <NavbarBrand href={"/" + this.user}>
                        <img src={logo} width={"50"}
                             height={"35"}/>
                    </NavbarBrand>
                    <Nav className="mr-auto" navbar>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle style={textStyle} nav caret>
                                Menu
                            </DropdownToggle>
                            <DropdownMenu right>
                                {
                                    (this.user === "doctor" || this.user === "caregiver")
                                    &&
                                    <DropdownItem>
                                        <NavLink href="/patient-management">Patients</NavLink>
                                    </DropdownItem>
                                }
                                {
                                    this.user === "doctor"
                                    &&
                                    <DropdownItem>
                                        <NavLink href="/caregiver-management">Caregivers</NavLink>
                                    </DropdownItem>
                                }
                                {
                                    this.user === "doctor"
                                    &&
                                    <DropdownItem>
                                        <NavLink href="/medication-management">Medication</NavLink>
                                    </DropdownItem>
                                }
                            </DropdownMenu>
                        </UncontrolledDropdown>

                    </Nav>
                </Navbar>
            </div>
        )
    }
}

export default NavigationBarContainer;
