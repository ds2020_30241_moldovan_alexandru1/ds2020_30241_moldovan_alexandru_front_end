import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Container, Jumbotron} from 'reactstrap';

import * as API_PATIENT from '../../src/person/api/patient-api';
import * as API_ACCOUNT from '../../src/person/api/account-api';
import * as API_CAREGIVER from '../../src/person/api/caregiver-api';
import NavigationBarContainer from "../navigation-bar-container";
import {Client} from "@stomp/stompjs";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white',};

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.account = null;
        this.role = '';
        this.state = {
            isLoaded: false,
            fetched: false,
            message: ''
        }
        if (this.props.match && !this.state.fetched) {
            this.getAccount(this.props.match.params.username);
        } else {
            if (!this.props.match) {
                this.role = 'doctor';
                this.setState({
                    isLoaded: true,
                    fetched: true
                });
            }
        }
    }

    getAccount(username) {
        return API_ACCOUNT.getAccountByUsername(username, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.role = result.role;
                if (result.role === 'patient') {
                    API_PATIENT.getPatientByUsername(username, (result, status, error) => {
                        if (result !== null && (status === 200 || status === 201)) {
                            this.account = result;
                            this.setState({
                                isLoaded: true,
                                fetched: true
                            });
                        } else {
                            this.setState(({
                                errorStatus: status,
                                error: error
                            }));
                        }
                    });
                }
                if (result.role === 'caregiver') {
                    API_CAREGIVER.getCaregiverByUsername(username, (result, status, error) => {
                        if (result !== null && (status === 200 || status === 201)) {
                            this.account = result;
                            this.setState({
                                isLoaded: true,
                                fetched: true
                            });
                        } else {
                            this.setState(({
                                errorStatus: status,
                                error: error
                            }));
                        }
                    });
                }
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    componentDidMount() {
        this.client = new Client();
        this.client.configure({
            brokerURL: 'ws://localhost:8080/stomp',
            onConnect: () => {
                console.log('onConnect');
                this.client.subscribe('/topic/greetings', message => {
                    this.setState({
                        message: this.state.message + ' ' + message.body + '\n'
                    });
                });
            },
        });

        this.client.activate();
    }

    render() {
        return (
            <div>
                {
                    (!this.state.isLoaded && this.role !== 'doctor')
                        ? <span>Loading</span>
                        :
                        <div>
                            {(this.role === "doctor" || this.role === "caregiver")
                            && <NavigationBarContainer user={this.role}/>}

                            <Jumbotron fluid style={backgroundStyle}>
                                <Container fluid>
                                    <h1 className="display-3"
                                        style={textStyle}>{this.role !== 'doctor' && this.account.name}</h1>
                                    <p className="lead" style={textStyle}>
                                        <b>{this.role !== 'doctor' && this.account.name}</b><br/>
                                        <b>{this.role !== 'doctor' && this.account.address}</b><br/>
                                        <b>{this.role !== 'doctor' && this.account.gender}</b><br/>
                                        {this.role === 'patient' && <b>{this.account.medicalRecord}</b>}
                                        {this.role === 'patient' && this.account.medicationPlanDTO &&
                                        <b>{this.account.medicationPlanDTO.period}</b>}
                                        {this.role === 'caregiver' && <b>{this.account.patients.length}</b>}
                                    </p>
                                    <p className="lead" style={textStyle}>
                                        {this.state.message}
                                    </p>
                                </Container>
                            </Jumbotron>
                        </div>
                }
            </div>

        )
    };
}

export default Home
