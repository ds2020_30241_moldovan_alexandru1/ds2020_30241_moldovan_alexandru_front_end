import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Home from './home/home';
import CommonContainer from './person/common-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import Login from "./login/index";
import CaregiverSensor from "./caregiverSensor";

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
                <Router>
                    <div>
                        <Switch>
                            <Route
                                exact
                                path='/doctor'
                                render={() => <Home role={"doctor"}/>}
                            />

                            <Route
                                exact
                                path='/patient/:username'
                                component={Home}/>
                            />

                            <Route
                                exact
                                path='/caregiver/:username'
                                component={Home}/>
                            />

                            <Route
                                exact
                                path='/patient-management'
                                render={() => <CommonContainer user={"patient"}/>}
                            />

                            <Route
                                exact
                                path='/caregiver-management'
                                render={() => <CommonContainer user={"caregiver"}/>}
                            />

                            <Route
                                exact
                                path='/medication-management'
                                render={() => <CommonContainer user={"medication"}/>}
                            />
                            {/*Error*/}
                            <Route
                                exact
                                path='/error'
                                render={() => <ErrorPage/>}
                            />

                            <Route
                                exact
                                path='/'
                                component={Login}
                            />
                            <Route
                                exact
                                path='/caregiver-sensor'
                                component={CaregiverSensor}>
                            </Route>

                            <Route render={() => <ErrorPage/>}/>
                        </Switch>
                    </div>
                </Router>
            </div>
        )
    };
}

export default App
